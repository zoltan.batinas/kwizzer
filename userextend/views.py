from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views.generic import CreateView
from userextend.forms import UserExtendForm
from django.shortcuts import render
from .forms import InputForm


class CreateUser(CreateView):
    template_name = 'userextend/create_user.html'
    model = User
    success_url = reverse_lazy('login')
    form_class = UserExtendForm


def home_view(request):
    context = {'form': InputForm()}
    return render(request, "home.html", context)
