from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.views.generic import TemplateView
from django.contrib.auth import authenticate, login


def home(request):
    return render(request, 'home/homepage.html')


class HomeTemplateView(TemplateView):
    template_name = 'home/homepage.html'


class LoginView(TemplateView):
    template_name = 'registration/login.html'


def login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('home')
        return HttpResponseRedirect
    else:
        render(request, 'registration/login.html')


def about(request):
    return render(request, 'about/about.html')
