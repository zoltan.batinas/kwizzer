from django.apps import AppConfig


class TopPlayersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'top_players'
