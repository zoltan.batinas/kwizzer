from django.contrib.auth.models import User
from django.db import models


class Quiz(models.Model):
    class Meta:
        verbose_name_plural = 'Quizzes'

    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Answer(models.Model):
    title = models.CharField(max_length=200)
    question = models.ForeignKey('Question', on_delete=models.CASCADE, related_name='answers')
    correct_answer = models.BooleanField()

    def __str__(self):
        return self.title


class Question(models.Model):
    title = models.CharField(max_length=200)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)

    # right_answer = models.OneToOneField(Answer, on_delete=models.CASCADE, related_name='+')
    # answers = models.ManyToManyField(Answer, related_name='answers')

    def __str__(self):
        return self.title


class QuizTaken(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    points = models.IntegerField(default=0)
