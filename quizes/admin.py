from django.contrib import admin

from quizes.models import Quiz, Question, Answer, QuizTaken


class AnswerAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(Quiz)
admin.site.register(Question)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(QuizTaken)
