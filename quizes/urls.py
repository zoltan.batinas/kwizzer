from django.urls import path

from quizes import views

urlpatterns = [
    path('quizzes/', views.quiz_list_view, name='quiz_list'),
    path('quiz/<int:pk>/', views.quiz_detail_view, name='quiz_detail'),
    path('quiz_taken/<int:pk>/', views.quiz_taken_detail_view, name='quiz_taken'),


]