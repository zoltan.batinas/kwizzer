from django.http import QueryDict
from django.shortcuts import render, redirect

from quizes.models import Quiz, Question, Answer, QuizTaken


def quiz_list_view(request):
    return render(request, 'quiz/quiz_list.html', {'quizzes': Quiz.objects.all()})


def quiz_detail_view(request, pk):
    quiz = Quiz.objects.get(id=pk)
    questions = Question.objects.filter(quiz=quiz)
    if request.method == 'POST':
        print(request.POST)
        post_dict = dict(request.POST)
        post_dict.pop('csrfmiddlewaretoken')
        points = 0
        for question_id in post_dict:
            answer = Answer.objects.get(id=post_dict[question_id][0])
            if answer.correct_answer:
                print('Correct')
                points += 1
        quiz_taken = QuizTaken.objects.create(user=request.user, quiz=quiz, points=points)
        return redirect(f'/quiz_taken/{quiz_taken.id}/')

    questions_with_answers = [{'question': q, 'answers': Answer.objects.filter(question=q)} for q in questions]
    return render(request, 'quiz/quiz_detail.html', {'quiz': quiz, 'questions_with_answers': questions_with_answers})


def quiz_taken_detail_view(request, pk):
    quiz_taken = QuizTaken.objects.get(pk=pk)
    return render(request, 'quiz/quiz_taken.html', {'quiz_taken': quiz_taken})
